const { ccclass, property } = cc._decorator;

@ccclass
export default class NodePoolComponent extends cc.Component {
    pool: cc.NodePool = null;
    put() {
        this.pool.put(this.node);
    }
    unuse() {
        // console.log(`${this.node.name}unuse`);
    }
    reuse() {
        // console.log(`${this.node.name}reuse`);
    }
};

/**
 * 创建写法：
 * 
    let npc: NodePoolComponent = this.pool.get()?.getComponent(NodePoolComponent);
    if (!npc) {
        npc = cc.instantiate(this.npcPrefab).getComponent(NodePoolComponent);
        npc.pool = this.pool;
        this.count++;
    }
    npc.node.name = `${this.count}`;
    this.root.addChild(npc.node);
    npc.node.setPosition(
        Random.range(0, 200),
        Random.range(0, 200),
    );
    npc.node.color = cc.color(
        Random.range(0, 255),
        Random.range(0, 255),
        Random.range(0, 255),
    );
    cc.tween(npc).delay(0.05).call(() => {
        npc.put();
    }).start();
 */