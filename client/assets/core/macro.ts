var Macro = {
    APP_NAME: "MXCGameFramework",
    APP_VERSION: "0",
    APP_VERSION_TIMESTAMP: "0",
    LRZ_QUALITY: 0.9,
    BUILD_TARGET: "dev",
    EVENTS: {
        DEBUG: "debug",

        ANI_MSG: "aniMsg",
        START_GAME: "startGame",
        PUSH_PAGE: "pushPage",
        PUSH_BOX: "pushBox",
        PUSH_NODE: "pushNode",

        MSG_BOX_OK: "msgBoxOk",
        MSG_BOX_YES: "msgBoxYes",
        MSG_BOX_NO: "msgBoxNo",

        WX_READY: "wxReady",
        USER_INFO_READY: "userInfoReady",
        USER_INFO_UPDATE: "userInfoUpdate",
        SHARE_OK: "shareOk",
        SHARE_CLOSE: "shareClose",

        SPLASH_OUT: "splashOut",
    },
    BUNDLE_VERSION: {
    }
};

export default Macro;