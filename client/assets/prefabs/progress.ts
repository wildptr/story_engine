import { SubjectComponent } from "../core/subject";

const { ccclass, property } = cc._decorator;


@ccclass("ProgressChanged")
export class ProgressChanged {
    prevValue: number = 0;
    value: number = 0;
};

@ccclass
export default abstract class Progress extends SubjectComponent {
    @property()
    minInfinity = false;
    @property({
        visible() {
            return !this.minInfinity;
        },
    })
    min = 0;

    @property()
    maxInfinity = false;
    @property({
        visible() {
            return !this.maxInfinity;
        },
    })
    max = 1;

    @property({
        serializable: true
    })
    _progress = 0;
    
    @property
    get progress(): number {
        return this._progress;
    }
    set progress(val: number) {
        let pc = new ProgressChanged();
        pc.prevValue = this._progress;
        this._progress = val;
        pc.value = this._progress;
        this.emit(ProgressChanged, pc);

        this.refresh();

    }

    abstract refresh(): void;
};