import Utils from "../../../core/utils";
import CheckBox from "../../../prefabs/check_box";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LoaderTest extends cc.Component {
    @property(cc.Prefab)
    groupPrefab: cc.Prefab = null;
    @property(cc.Node)
    container: cc.Node = null;
    protected start(): void {
        Utils.loader.groupMap.forEach((arr, group) => {
            let item = cc.instantiate(this.groupPrefab);
            item.getComponent(cc.Label).string = group;
            let contain = item.children[0].children[0].children[0];
            let cPrefab = contain.children[0];
            cPrefab.removeFromParent(false);

            arr.forEach(ah => {
                let cItem = cc.instantiate(cPrefab);
                cItem.getComponent(cc.RichText).string = `<color=#00ff00>${ah.name}</c>\n<color=#0fffff>${cc.js.getClassName(ah.asset)}</color>`;
                contain.addChild(cItem);

                if (cc.js.getClassName(ah.asset) === "cc.Texture2D") {
                    let node = new cc.Node(ah.name);
                    contain.addChild(node);
                    node.width = 200;
                    node.height = 100;
                    node.anchorX = 0;
                    let sp = node.addComponent(cc.Sprite);
                    sp.sizeMode = cc.Sprite.SizeMode.CUSTOM;
                    sp.trim = false;
                    sp.type = cc.Sprite.Type.SIMPLE;
                    sp.spriteFrame = new cc.SpriteFrame(<cc.Texture2D>ah.asset);
                }
            });

            this.container.addChild(item);
        });

        this.getComponentsInChildren(CheckBox).forEach(cb => {
            cb.on("clicked", () => {
                if (this.curCb) {
                    this.curCb.checked = false;
                }
                this.curCb = cb;
            }, this);
        })
    }
    curCb: CheckBox = null;

    onClickClose() {
        this.node.destroy();
    }
};