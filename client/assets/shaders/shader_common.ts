import { ShaderAttribute } from "../core/data_ext";
import ShaderBase from "./shader_base";

const { ccclass, property } = cc._decorator;


export enum ShaderAttributeDataType {
    None,
    Float,
    Vec2,
    Vec3,
    Vec4,
};

@ccclass("ShaderAttributeSlot")
export class ShaderAttributeData {
    @property({
        type: cc.Enum(ShaderAttributeDataType)
    })
    type: ShaderAttributeDataType = ShaderAttributeDataType.None;

    @property({
        visible() {
            return this.type !== ShaderAttributeDataType.None;
        },
    })
    key: string = "";
    @property({
        visible() {
            return this.type === ShaderAttributeDataType.Float;
        },
    })
    float: number = 0;
    @property({
        visible() {
            return this.type === ShaderAttributeDataType.Vec2;
        },
    })
    vec2: cc.Vec2 = cc.v2();
    @property({
        visible() {
            return this.type === ShaderAttributeDataType.Vec3;
        },
    })
    vec3: cc.Vec3 = cc.v3();
    @property({
        visible() {
            return this.type === ShaderAttributeDataType.Vec4;
        },
    })
    vec4: cc.Vec4 = new cc.Vec4();
};


@ccclass
export default class ShaderCommon extends ShaderBase {
    @property([ShaderAttributeData])
    datas: ShaderAttributeData[] = [];
    refresh() {
        this.datas.forEach(data => {
            let val: ShaderAttribute = null;
            switch (data.type) {
                case ShaderAttributeDataType.Float:
                    val = data.float;
                    break;
                case ShaderAttributeDataType.Vec2:
                    val = data.vec2;
                    break;
                case ShaderAttributeDataType.Vec3:
                    val = data.vec3;
                    break;
                case ShaderAttributeDataType.Vec4:
                    val = data.vec4;
                    break;
                default:
                case ShaderAttributeDataType.None:
                    return;
                    break;
            }
            this.material.setProperty(data.key, val);
        });
    }
};