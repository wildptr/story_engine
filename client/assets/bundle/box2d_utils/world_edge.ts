const { executeInEditMode, ccclass, property } = cc._decorator;

@ccclass
@executeInEditMode
export default class WorldEdge extends cc.Component {
    @property(cc.PhysicsPolygonCollider)
    get polyCollider() {
        return this.getComponent(cc.PhysicsPolygonCollider) || this.addComponent(cc.PhysicsPolygonCollider);
    }
    updateEdge() {
        let w2 = this.node.width / 2;
        let h2 = this.node.height / 2;
        let pts = [
            cc.v2(-w2 - 300, -h2 - 300),
            cc.v2(w2 + 300, -h2 - 300),
            cc.v2(w2 + 300, h2 + 300),
            cc.v2(-w2 - 300, h2 + 300),

            cc.v2(-w2, h2),
            cc.v2(w2, h2),
            cc.v2(w2, -h2),
            cc.v2(-w2, -h2),
            cc.v2(-w2, h2),
            cc.v2(-w2 - 300, h2 + 300),
        ];
        this.polyCollider.points = pts;
        this.polyCollider.apply();
    }
    lateUpdate(dt: number) {
        if (CC_EDITOR) {
            this.updateEdge();
        }
    }
}
