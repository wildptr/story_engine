import { ReplacePageAni } from "../../../../core/game";
import Utils, { Sync } from "../../../../core/utils";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PageBulletTest extends cc.Component {
    protected onEnable(): void {
        cc.director.getCollisionManager().enabled = true;
        Utils.game.sceneCamera.enabled = false;
    }
    protected onDisable(): void {
        cc.director.getCollisionManager().enabled = false;
        Utils.game.sceneCamera.enabled = true;
    }
    async onClickReset() {
        this.node.destroy();
        await Sync.DelayTime(0.2);
        Utils.game.resReplacePage({
            name: "page_bullet_test",
            ani: ReplacePageAni.None
        });
    }
}
