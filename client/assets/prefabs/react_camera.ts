const { executeInEditMode, ccclass, property } = cc._decorator;

@ccclass
@executeInEditMode
export default class ReactCamera extends cc.Component {
    _cam: cc.Camera = null;
    @property(cc.Camera)
    get cam() {
        if (!this._cam) {
            this._cam = this.getComponent(cc.Camera) || this.addComponent(cc.Camera);
        }
        return this._cam;
    }
    @property(ReactCamera)
    target: ReactCamera = null;
    @property
    reactInUpdate = true;

    @property({
        serializable: true,
        visible: false
    })
    _world = false;
    @property
    get world() {
        return this._world;
    }
    set world(val) {
        this._world = val;
        this.onChanged();
    }

    @property
    effectPos = false;
    @property
    _effectPosScale = 1.0;
    @property({
        visible() {
            return this.effectPos;
        },
    })
    get effectPosScale() {
        return this._effectPosScale;
    }
    set effectPosScale(val) {
        this._effectPosScale = val;
        this.onChanged();
    }

    @property({
        visible() {
            return this.effectPos;
        }
    })
    offsetPos: cc.Vec2 = new cc.Vec2();


    @property
    effectSize = false;
    @property
    _effectSizeScale = 1.0;
    @property({
        visible() {
            return this.effectSize;
        },
    })
    get effectSizeScale() {
        return this._effectSizeScale;
    }
    set effectSizeScale(val) {
        this._effectSizeScale = val;
        this.onChanged();
    }

    @property({
        visible() {
            return this.effectSize;
        }
    })
    offsetSize: cc.Vec2 = new cc.Vec2();

    @property
    effectScale = false;
    @property
    _effectScaleScale = 1.0;
    @property({
        visible() {
            return this.effectScale;
        },
    })
    get effectScaleScale() {
        return this._effectScaleScale;
    }
    set effectScaleScale(val) {
        this._effectScaleScale = val;
        this.onChanged();
    }

    // @property({
    //     visible() {
    //         return this.effectScale;
    //     }
    // })
    // offsetSize: cc.Vec2 = new cc.Vec2();


    protected onLoad(): void {
        this.node.on("size-changed", this.onOrthoChanged, this);
        this.node.on("scale-changed", this.onOrthoChanged, this);

        if (this.target) {
            this.setTarget(this.target);
        }
    }
    onOrthoChanged() {
        this.cam.orthoSize = this.node.height / 2 * this.node.scale;
    }

    setTarget(target: ReactCamera) {
        if (cc.isValid(this.target)) {
            this.target.node.off("position-changed", this.onChanged, this);
            this.target.node.off("active-in-hierarchy-changed", this.onChanged, this);
            this.target.node.off("size-changed", this.onChanged, this);
            this.target.node.off("scale-changed", this.onChanged, this);
        }

        this.target = target;
        if (cc.isValid(this.target)) {
            this.target.node.on("position-changed", this.onChanged, this);
            this.target.node.on("active-in-hierarchy-changed", this.onChanged, this);
            this.target.node.on("size-changed", this.onChanged, this);
            this.target.node.on("scale-changed", this.onChanged, this);
            this.onChanged();
        }
    }
    onChanged() {
        if (!this.target) {
            return;
        }
        if (this.effectPos) {
            let pos = null;
            if (this._world) {
                pos = this.target.node.convertToWorldSpaceAR(cc.Vec2.ZERO);
                pos = this.node.parent.convertToNodeSpaceAR(pos);
            }
            else {
                pos = this.target.node.position;
            }
            pos = pos.add(this.offsetPos).mul(this._effectPosScale);
            this.node.setPosition(pos);
        }
        if (this.effectSize) {
            this.node.width = this.target.node.width * this._effectSizeScale + this.offsetSize.x;
            this.node.height = this.target.node.height * this._effectSizeScale + this.offsetSize.y;
        }
        if (this.effectScale) {
            let deltaX = (this.target.node.scaleX - 1) * this._effectScaleScale;
            this.node.scaleX = 1 + deltaX;
            let deltaY = (this.target.node.scaleY - 1) * this._effectScaleScale;
            this.node.scaleY = 1 + deltaY;
        }
    }

    protected lateUpdate(dt: number): void {
        if (CC_EDITOR || this.reactInUpdate) {
            this.onChanged();
        }
    }
};