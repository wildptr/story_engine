const RES_DOMAIN = "asiamiles-h5.otype.com/plant/";
const RES_H5_URL = "https://h5.otype.com/plant/";

const TEST_DOMAIN = "asiamiles-h5-test.cojoy.com.cn/plant/";
const TEST_H5_URL = "https://h5-test.cojoy.com.cn/plant/";

const TITLE = "主页";
const OPTION = {
    hasEruda: true,
    hasJQ: true,
    hasWx: true,
    showVersion: true,
};
//----
Date.prototype.format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1,                 //月份 
        "d+": this.getDate(),                    //日 
        "h+": this.getHours(),                   //小时 
        "m+": this.getMinutes(),                 //分 
        "s+": this.getSeconds(),                 //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds()             //毫秒 
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
}
const APP_VERSION = (new Date()).format("yyyy-MM-dd hh:mm:ss");
const APP_VERSION_TIMESTAMP = Date.now().toString();
const TARGET_EN_CN = {
    "test": "内部测试版本，中奖请勿当真。版本：",
    "res": "线上:"
};
import fs from 'fs';
var buildTarget = process.argv[2];
var rootPath = "build/web-mobile/";
const FILE_JS_PATH = `${rootPath}assets/main/index.js`;
const FILE_INDEX_PATH = `${rootPath}index.html`;

{
    let fileStr = fs.readFileSync(FILE_JS_PATH).toString();

    fileStr = fileStr.replace(/APP_VERSION\s*:\s*"(.+?)"/g, `APP_VERSION:"${TARGET_EN_CN[buildTarget]}-${APP_VERSION}"`);
    fileStr = fileStr.replace(/APP_VERSION_TIMESTAMP\s*:\s*"(.+?)"/g, `APP_VERSION_TIMESTAMP:"${APP_VERSION_TIMESTAMP}"`);
    fileStr = fileStr.replace(/DEBUG\s*:\s*\!0/g, `DEBUG:0`);
    fileStr = fileStr.replace(/DEBUG\s*:\s*1/g, `DEBUG:0`);

    switch (buildTarget) {
        case "res":
            fileStr = fileStr.replace(/SERVER_DOMAIN\s*:\s*"(.+?)"/g, `SERVER_DOMAIN:"${RES_SERVER_DOMAIN}"`);
            fileStr = fileStr.replace(/DOMAIN\s*:\s*"(.+?)"/g, `DOMAIN:"${RES_DOMAIN}"`);
            fileStr = fileStr.replace(/H5_URL\s*:\s*"(.+?)"/g, `H5_URL:"${RES_H5_URL}"`);
            break;
        case "test":
            fileStr = fileStr.replace(/SERVER_DOMAIN\s*:\s*"(.+?)"/g, `SERVER_DOMAIN:"${TEST_SERVER_DOMAIN}"`);
            fileStr = fileStr.replace(/DOMAIN\s*:\s*"(.+?)"/g, `DOMAIN:"${TEST_DOMAIN}"`);
            fileStr = fileStr.replace(/H5_URL\s*:\s*"(.+?)"/g, `H5_URL:"${TEST_H5_URL}"`);
            break;
    }
    fs.writeFileSync(FILE_JS_PATH, fileStr);
}

{
    let paths = [
        FILE_INDEX_PATH,
        // FILE_INDEX_ERUDA_PATH,
    ];
    for (let i = 0; i < paths.length; i++) {
        let path = paths[i];
        let fileStr = fs.readFileSync(path).toString();
        fileStr = fileStr.replace("{{title}}", TITLE);
        fileStr = fileStr.replace("{{jweixin}}",
            OPTION.hasWx ?
                `<script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.3.2.js"></script><script src="js/platform.js" charset="utf-8"></script>`
                : ``
        );
        fileStr = fileStr.replace("{{eruda}}",
            OPTION.hasEruda && buildTarget == "test" ?
                `<script src="js/eruda.min.js" charset="utf-8"></script>`
                : ``
        );
        fileStr = fileStr.replace("{{jq}}",
            OPTION.hasJQ ?
                `<script src="js/jquery.min.js" charset="utf-8"></script>`
                : ``
        );
        fileStr = fileStr.replace("{{show_version}}",
            OPTION.showVersion && buildTarget == "test" ?
                `<script type="text/javascript">window.SHOW_VERSION = true</script>`
                :
                `<script type="text/javascript">window.SHOW_VERSION = false</script>`
        );

        fs.writeFileSync(path, fileStr);
    }
}
