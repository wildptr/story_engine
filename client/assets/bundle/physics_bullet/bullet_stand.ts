import { SubjectComponent } from "../../core/subject";
import { Sync } from "../../core/utils";
import Bullet from "./bullet";

const { ccclass, property } = cc._decorator;

@ccclass
export default class BulletStand extends SubjectComponent {
    col: cc.BoxCollider = null;
    self: Bullet = null;
    other: Bullet = null;

    colHeight = 0;
    colHeightDelta = 0;
    detaching = false;

    staticStand = false; //静态，需要手动删除
    init(self: Bullet, land: Bullet) {
        this.self = self;
        this.other = land;
        this.col = this.addComponent(cc.BoxCollider);
        this.colHeight = self.col.size.height + 50;

        this.col.size.width = self.col.size.width;
        this.col.size.height = this.colHeight + Math.abs(this.other.deltaPos.y * 2);
    }

    protected lateUpdate(dt: number): void {
        if (!cc.isValid(this) || !cc.isValid(this.other)) {
            this.node.destroy();
            return;
        }
        this.col.size.height = this.colHeight + Math.abs(this.other.deltaPos.y * 2);
    }

    protected onDestroy(): void {
        this.emit("leave");
        this.detaching = true;
    }

    onCollisionExit(other: cc.BoxCollider, self: cc.BoxCollider) {
        if (other === this.other.col) {
            Sync.NextUpdate().then(() => {
                if (cc.isValid(this)) {
                    this.node.destroy();
                }
            });
        }
    }
};