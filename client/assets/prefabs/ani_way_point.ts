const { ccclass, property } = cc._decorator;

export enum WayPointPlayType {
    Normal,
    Loop,
    PingPong,
};

@ccclass
export default class AniWayPoint extends cc.Component {
    @property(cc.Node)
    wayPointRoot: cc.Node = null;
    @property
    totalTime = 2;

    @property
    angle = false;

    @property
    position = true;

    @property
    opacity = false;

    @property
    scale = false;

    @property
    startInd = 0;

    @property
    easing = "sineInOut";

    @property({
        type: cc.Enum(WayPointPlayType)
    })
    playType: WayPointPlayType = WayPointPlayType.Normal;

    protected start(): void {
        if (this.wayPointRoot.childrenCount < 2) {
            this.destroy();
            return;
        }
        //将waypoint按照自定义开始点重新排列
        let list = this.wayPointRoot.children.slice();
        list = list.slice(this.startInd).concat(list.slice(0, this.startInd));
        let pt0 = list[0];

        this.setToNode(pt0);
        // this.node.setPosition(pt0.position);

        let act = cc.tween(this.node);

        let stepTime = this.totalTime / list.length;
        for (let i = 1; i < list.length; i++) {
            let pt = list[i];
            this.lerpToNode(act, stepTime, pt);
        }

        switch (this.playType) {
            case WayPointPlayType.Normal:
            case WayPointPlayType.Loop:
                this.lerpToNode(act, stepTime, pt0);
                break;
            case WayPointPlayType.PingPong:
                for (let i = list.length - 2; i >= 0; i--) {
                    let pt = list[i];
                    this.lerpToNode(act, stepTime, pt);
                }
                break;
        }

        if (this.playType !== WayPointPlayType.Normal) {
            cc.tween(this.node).repeatForever(act).start();
        }
        else {
            act.start();
        }
    }
    setToNode(node: cc.Node) {
        let prop: any = {};
        if (this.position) {
            prop.x = node.x;
            prop.y = node.y;
        }
        if (this.angle) {
            prop.angle = node.angle;
        }
        if (this.opacity) {
            prop.opacity = node.opacity;
        }

        Object.assign(this.node, prop);
    }
    lerpToNode(act: cc.Tween, stepTime: number, node: cc.Node) {
        let prop: any = {};
        if (this.position) {
            prop.x = node.x;
            prop.y = node.y;
        }
        if (this.angle) {
            prop.angle = node.angle;
        }
        if (this.opacity) {
            prop.opacity = node.opacity;
        }
        if (this.scale) {
            prop.scaleX = node.scaleX;
            prop.scaleY = node.scaleY;
        }

        act.to(stepTime, prop, { easing: this.easing });

    }
};