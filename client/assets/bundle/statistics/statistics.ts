import { ProtocolC2SStat } from "./statistics_protocol_dist";
export var BUNDLE_STATISTICS_BUILD_TARGET: "dev" | "test" | "res" = "dev";
export default class Statistics {
    static tableName = "";
    static event(type: string, str: string) {
        if (!this.tableName) {
            console.error("Statistics::event, error:未设置表名");
            return;
        }
        let url = "";
        if (cc.sys.isNative) {
            url = "https:"
        }
        else {
            url = window.location.protocol;
        }
        switch (BUNDLE_STATISTICS_BUILD_TARGET) {
            case "dev":
            case "test":
                url = `${url}//local.gameptr.com`
                break;
            case "res":
                url = `${url}//gameptr.com`
                break;
        }

        url = `${url}/project/statistics/server/req.do`;


        let msg = new ProtocolC2SStat();
        msg.table_name = this.tableName;
        msg.data = encodeURIComponent(JSON.stringify({
            type: type,
            event: str,
        }));

        let dat: any = {
            action: "add_stat",
        };
        Object.assign(dat, msg.toMixed());

        let req = HttpRequest.Get(url);
        req.setParam(dat);
        req.reqPromise();
    }
};


type NetData = boolean | number | string;
//todo: 完善请求属性，满足更多需求。
class HttpRequest {
    url: string = "";
    urlRaw: string = "";
    type: string = "GET";
    rspType: XMLHttpRequestResponseType = "";
    contentType: "application/json" | "application/x-www-form-urlencoded" | "multipart/form-data";
    param: { [key: string]: NetData } = {};
    body: any = null;
    headers: { [key: string]: string } = {
        "Accept": "*/*",
    };
    timeout: number = 5000;
    async: boolean = true;

    static Post(pathKey: string) {
        let req = new HttpRequest(pathKey);
        req.type = "POST";
        req.contentType = "application/json";
        return req;
    }
    static Get(pathKey: string) {
        let req = new HttpRequest(pathKey);
        req.type = "GET";
        req.contentType = "application/x-www-form-urlencoded";
        return req;
    }
    constructor(path?: string) {
        if (path) {
            this.urlRaw = path;
        }
    }

    setBody(body: any) {
        this.body = body;
        return this;
    }
    setParam(param: { [key: string]: NetData }) {
        this.param = param;
        return this;
    }
    setHeader(headers: { [key: string]: string }) {
        this.headers = headers;
        return this;
    }

    reqPromise() {
        return new Promise<any>((ok, fail) => {
            Http.req(this, ok, fail);
        });
    }
};
class Http {
    static req(option: HttpRequest, success, error) {
        let request = new XMLHttpRequest();
        request.timeout = option.timeout;
        //Can be set to change the response type. Values are: the empty string (default), "arraybuffer", "blob", "document", "json", and "text".
        request.responseType = option.rspType;
        let reqUrl = option.urlRaw;

        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                let status = request.status;
                let rspSuccess = true;
                if (status >= 200 && status < 400) {
                    let rsp: any = request.response;
                    if (option.rspType == "") { //默认是读取字符串
                        try {
                            rsp = JSON.parse(request.responseText);
                        }
                        catch (e) {
                            console.warn("Http::req, warn:", "返回的数据JSON解析失败", e);
                            rspSuccess = false;
                        }
                    }

                    if (rsp && rspSuccess) {
                        console.log(`http, ${reqUrl}, ok:`, rsp);
                        success && success(rsp);
                    }
                    else {
                        console.log(`http, ${reqUrl}, fail:`, rsp);
                        error && error(rsp);
                        return;
                    }

                }
                else {
                    error(status);
                }
            }
        };

        request.onerror = function () {
            error("net-error");
        };

        request.ontimeout = function () {
            error("timeout");
        };

        var body = null;
        if (option.body) {
            body = option.body;
        }

        if (option.param) {
            if (body && option.type === "POST" && option.contentType === "application/x-www-form-urlencoded") {
                Object.assign(option.param, option.body);
            }
            let sim = "?";
            for (let key in option.param) {
                reqUrl += sim + key + "=" + option.param[key];
                sim = "&";
            }
        }

        request.open(option.type, reqUrl, option.async);

        if (option.headers) {
            for (let key in option.headers) {
                request.setRequestHeader(key, option.headers[key]);
            }
            request.setRequestHeader("Content-Type", option.contentType);
        }



        if (body) {
            switch (option.contentType) {
                case "application/json":
                    request.send(JSON.stringify(body));
                    break;
                case "application/x-www-form-urlencoded":
                    request.send();
                    break;
                case "multipart/form-data":
                    request.send(body);
                    break;
            }
        }
        else {
            request.send();
        }
    }
};

window["statistics"] = Statistics;