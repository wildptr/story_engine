import { c2n } from "./serialize";

export class SyncMessage {
    private __syncCallback: () => void = null;
    async() {
        return new Promise<void>(ok => {
            this.__syncCallback = ok;
        });
    }
    finish() {
        this.__syncCallback && this.__syncCallback();
        this.__syncCallback = null;
    }
};

export class Subject {
    private _obs: SubjectListener[] = [];
    clean() {
        this._obs = [];
    }
    once<T>(eventName: string | { new(): T }, callback: (msg: any, eventName: string) => void, target: any): SubjectListener {
        let ls = this.on(eventName, callback, target);
        ls.isOnce = true;
        return ls;
    }
    on<T>(eventName: string | { new(): T }, callback: (msg: T, eventName: string) => void, target: any): SubjectListener {
        if (typeof eventName !== "string") {
            eventName = c2n(eventName) || eventName.name;
        }
        let sl = new SubjectListener();
        sl.eventName = eventName;
        sl.callback = callback;
        sl.target = target;
        sl.isOnce = false;
        this._obs.push(sl);
        return sl;
    }
    off<T>(target: any, eventName?: string | { new(): T }, callback?: (msg: T, eventName: string) => void): void {
        if (eventName && typeof eventName !== "string") {
            eventName = c2n(eventName) || eventName.name;
        }
        for (var i = 0; i < this._obs.length; i++) {
            let listener = this._obs[i];
            if (eventName && listener.eventName != eventName) {
                continue;
            }
            if (callback && listener.callback != callback) {
                continue;
            }

            if (listener.target === target) {
                this._obs.splice(i, 1);
                i--;
            }
        }
    }
    emit<T>(eventName: string | { new(): T }, msg?: T) {
        if (typeof eventName !== "string") {
            eventName = c2n(eventName) || eventName.name;
        }
        for (var i = 0; i < this._obs.length; i++) {
            let listener = this._obs[i];
            if (listener.eventName === eventName && listener.callback) {
                listener.callback.call(listener.target, msg, eventName);
                if (listener.isOnce) {
                    this._obs.splice(i, 1);
                    i--;
                }
            }
        }
    }
    emitSync<T extends SyncMessage>(eventName: string | { new(): T }, msg: T) {
        return new Promise<void>(ok => {
            msg.async().then(ok);
            this.emit(eventName, msg);
        });
    }
};

export class SubjectListener {
    eventName: string;
    callback: (msg: any, eventName: string) => void;
    target: any;
    isOnce: boolean;
};

const { ccclass, property } = cc._decorator;
@ccclass
export class SubjectComponent extends cc.Component {
    private _subject: Subject = new Subject();

    once<T>(eventName: string | { new(): T }, callback: (msg: any, eventName: string) => void, target: any): SubjectListener {
        return this._subject.once(eventName, callback, target);
    }

    on<T>(eventName: string | { new(): T }, callback: (msg: T, eventName: string) => void, target: any): SubjectListener {
        return this._subject.on(eventName, callback, target);
    }

    off<T>(target: any, eventName?: string | { new(): T }, callback?: (msg: T, eventName: string) => void): void {
        return this._subject.off(target, eventName, callback);
    }

    emit<T>(eventName: string | { new(): T }, msg?: T) {
        this._subject.emit(eventName, msg);
    }
    emitSync<T extends SyncMessage>(eventName: string | { new(): T }, msg?: T) {
        return this._subject.emitSync(eventName, msg);
    }
};

let MsgHub = new Subject();
export default MsgHub;