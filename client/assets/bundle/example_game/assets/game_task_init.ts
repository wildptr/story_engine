import GameTask from "../../../core/game_task";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameTaskAsiaInit extends GameTask {
    async init() {
        console.log("game_task_init 运行成功");
        this.onFinished();
    }
};  