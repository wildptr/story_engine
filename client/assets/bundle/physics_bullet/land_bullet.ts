import Bullet from "./bullet";

const { executeInEditMode, ccclass, property } = cc._decorator;


@ccclass
@executeInEditMode
export default class LandBullet extends Bullet {
    @property
    get editorX() {
        return this.node.x / 100;
    }
    set editorX(val) {
        this.node.x = val * 100;
        this.editorUpdateCol();
    }
    @property
    get editorY() {
        return this.node.y / 100;
    }
    set editorY(val) {
        this.node.y = val * 100;
        this.editorUpdateCol();
    }
    @property
    get editorHeight() {
        return this.node.height / 100;
    }
    set editorHeight(val) {
        this.node.height = val * 100;
        this.editorUpdateCol();
    }
    @property
    get editorWidth() {
        return this.node.width / 100;
    }
    set editorWidth(val) {
        this.node.width = val * 100;
        this.editorUpdateCol();
    }
    editorUpdateCol() {
        this.col.size.width = this.node.width;
        this.col.size.height = this.node.height;
    }

    onGrab(other: Bullet): void {

    }
};