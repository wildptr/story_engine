import Utils from "../core/utils";

const { executeInEditMode, ccclass, property } = cc._decorator;

@ccclass
export default class FrameBuffer extends cc.Component {
    _cam: cc.Camera = null;
    @property(cc.Camera)
    get cam() {
        if (!this._cam) {
            let camNode = cc.find("cam", this.node);
            if (!camNode) {
                camNode = new cc.Node("cam");
                this.node.addChild(camNode);
            }

            this._cam = camNode.getComponent(cc.Camera) || camNode.addComponent(cc.Camera);
        }
        return this._cam;
    }
    _sprite: cc.Sprite = null;
    @property(cc.Sprite)
    get sprite() {
        if (!this._sprite) {
            let spNode = cc.find("sprite", this.node);
            if (!spNode) {
                spNode = new cc.Node("sprite");
                this.node.addChild(spNode);
            }

            this._sprite = spNode.getComponent(cc.Sprite) || spNode.addComponent(cc.Sprite);
        }
        return this._sprite;
    }

    //对于目标有mask时，初始化renderTexture必须使用RB_FMT_S8格式。
    @property
    maskFix = false;
    @property
    ratio = 1;

    @property(cc.Node)
    target: cc.Node = null;
    @property
    fitWithTarget = false;

    protected onLoad(): void {
        this.sprite.node.scaleY = -1;
        this.node.on("size-changed", this.onSizeChanged, this);
        this.setTarget(this.target);
    }

    setTarget(target: cc.Node) {
        this.target = target;
        this.initTexture();
    }

    initTexture() {
        let tex = new cc.RenderTexture();


        let texWidth = this.node.width * this.ratio;
        let texHeight = this.node.height * this.ratio;

        if (this.target && this.fitWithTarget ) {
            texWidth = this.target.width * this.ratio;
            texHeight = this.target.height * this.ratio;
        }

        if (this.maskFix) {
            tex.initWithSize(texWidth, texHeight, cc.RenderTexture.DepthStencilFormat.RB_FMT_S8);
        }
        else {
            tex.initWithSize(texWidth, texHeight);
        }
        if (this.sprite.spriteFrame) {
            this.sprite.spriteFrame.setTexture(tex);
        }
        else {
            this.sprite.spriteFrame = new cc.SpriteFrame(tex);
        }

        if (this.cam.alignWithScreen) {
            this.cam.zoomRatio = Utils.game.node.height / texHeight ;
        }
        else {
            this.cam.orthoSize = texHeight / 2; 
        }
        this.cam.targetTexture = tex;
        this.cam.enabled = false;
    }

    onSizeChanged() {
        this.sprite.node.width = this.node.width * this.ratio;
        this.sprite.node.height = this.node.height * this.ratio;
        // this.sprite.node.width = this.sprite.node.width;
        // this.sprite.node.height = this.sprite.node.height;
        this.initTexture();
    }

    render(node: cc.Node) {
        this.cam.render(node);
    }

    update(dt: number) {
        if (this.target) {
            this.cam.render(this.target);
        }
    }
};