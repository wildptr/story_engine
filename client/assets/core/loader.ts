import JSZip from "jszip";
import { MsgBoxType } from "../prefabs/msg_box";
import AssetHandleNode, { AssetHandle } from "./asset_handle_node";
import data, { CacheData } from "./cache_data";
import { Col, Collection } from "./data_ext";
import { Http } from "./http_request";
import Macro from "./macro";
import NativeLogger from "./native_logger";
import SerializeAble, { Serialize } from "./serialize";
import Utils from "./utils";
import { Subject } from "./subject";

const { ccclass, property } = cc._decorator;


export enum PreloadType {
    Bundle = 1,
    Remote = 2
};
export class PreloadConfig {
    path: string = "";
    group: string = "";
    type: string = "";
    preloadType: PreloadType = PreloadType.Bundle;

    constructor(path, group, type, preloadType: PreloadType = PreloadType.Bundle) {
        this.path = path;
        this.group = group;
        this.type = type;
        this.preloadType = preloadType;
    }
    static GetNameByPath(path: string) {
        let name = "";
        name = path.substring(path.lastIndexOf("/") + 1);
        //3.x 版本，spriteFrame texture 是子资源。
        // do {
        //     name = path.substring(path.lastIndexOf("/") + 1);
        //     if (name === "spriteFrame" || name === "texture") {
        //         path = path.substring(0, path.lastIndexOf("/"));
        //     }
        //     else {
        //         break;
        //     }
        // }
        // while (1);
        return name;
    }
};

const DefaultLoad = [
    new PreloadConfig("pages/", "pages", "cc.Prefab"),
    new PreloadConfig("boxes/", "boxes", "cc.Prefab"),
    new PreloadConfig("prefabs/", "prefabs", "cc.Prefab"),
    new PreloadConfig("sounds/", "sounds", "cc.AudioClip"),
    new PreloadConfig("others/", "others", "")
];

export enum LoaderState {
    Idle = 1,
    CheckUpdate,
    Updating,
    LoadBundle,
    LoadBundleOk,
    PreloadAsset,
};
export class BundleConfig extends SerializeAble {
    @Serialize()
    name: string = "";
    @Serialize()
    url: string = "";
    @Serialize()
    version: string = "";
    @Serialize()
    index = 0;
    @Serialize()
    preload: PreloadConfig[] = [];
};
export class BundleInfo {
    config: BundleConfig;
    from: "origin" | "local" | "web" = "origin";
    abPath: string = "";
    isLoaded = false;
};


export type BundleConfigCollection = Collection<BundleConfig>;
export interface CheckUpdateResult {
    versionRemote: BundleConfigCollection;
    versionLocal: BundleConfigCollection;
    needUpdateList: string[];
};

abstract class LoadPipe extends Subject {
    config: PreloadConfig;
    static CheckAsset(ctor: typeof cc.Asset) {
        //@ts-ignore
        return ctor instanceof cc.Asset && ctor != cc.Texture2D;
    }
    abstract load(): Promise<void>;
};

class LoadPipeRemote extends LoadPipe {
    assetName = "";
    load(): Promise<void> {
        return new Promise<void>(ok => {
            cc.assetManager.loadRemote(this.config.path, (err, asset: cc.Asset) => {
                if (err) {
                    this.emit("fail", err);
                }
                else {
                    let ah = new AssetHandle();
                    ah.asset = asset;
                    ah.name = this.assetName;
                    ah.path = this.config.path;
                    ah.group = this.config.group;
                    this.emit("success", ah);
                }
                ok();
            });
        });
    }
};

class LoadPipeBundle extends LoadPipe {
    bundle: cc.AssetManager.Bundle;

    load() {
        return new Promise<void>(ok => {
            this.bundle.load(this.config.path, cc.Asset, (err, asset: cc.Asset) => {
                if (err) {
                    this.emit("fail", err);
                }
                else {
                    let assetName = PreloadConfig.GetNameByPath(this.config.path);
                    let ah = new AssetHandle();
                    ah.asset = asset;
                    ah.name = assetName;
                    ah.path = this.config.path;
                    ah.group = this.config.group;
                    this.emit("success", ah);
                }
                ok();
            });
        });
    }
};

// const DefaultPreloadDir = ["pages", "boxes", "sounds"];
// const DefaultType = [cc.Prefab, cc.Prefab, cc.AudioClip];

@ccclass
export default class Loader extends AssetHandleNode {
    @property({
        type: [AssetHandle],
        override: true,
        visible: false
    })
    handles: AssetHandle[] = [];
    @property([AssetHandle])
    boxes: AssetHandle[] = [];
    @property([AssetHandle])
    pages: AssetHandle[] = [];
    @property([AssetHandle])
    sounds: AssetHandle[] = [];

    // protected _bundleAssetNameMap: Collection<Collection<AssetHandle>> = {};
    // protected _bundleAssetPathMap: Collection<Collection<AssetHandle>> = {};

    protected onDestroy(): void {
        Loader.__ins = null;
        Utils.loader = null;
    }
    protected onLoad(): void {
        super.onLoad && super.onLoad();
        Loader.__ins = this;
        Utils.loader = this;
        //线程，并发
        // if (cc.sys.isNative) {
        //     cc.assetManager.downloader.maxConcurrency = 2;
        //     cc.assetManager.downloader.maxRequestsPerFrame = 2;
        // }
    }
    remap() {
        this.bundles = [];
        // this._bundleAssetNameMap = {};
        // this._bundleAssetPathMap = {};
        this._assetNameMap.clear();
        this._assetTypeMap.clear();
        this._assetGroupMap.clear();
        this._assetPathMap.clear();
        this.handles = []; //todo: loader 的handles 是临时保存AssetHandle。与 AssetHandleNode 中的handles意义不同。
        this.pages.forEach(ah => {
            ah.group = "pages";
            this.remapOne(ah);
        });
        this.boxes.forEach(ah => {
            ah.group = "boxes";
            this.remapOne(ah);
        });
        this.sounds.forEach(ah => {
            ah.group = "sounds";
            this.remapOne(ah);
        });
    }
    private static __ins = null;
    static get ins(): Loader {
        return this.__ins;
    }

    @property(cc.JsonAsset)
    originVersionJson: cc.JsonAsset = null;
    state: LoaderState = LoaderState.Idle;

    //bundle
    bundles: cc.AssetManager.Bundle[] = [];
    //bundle
    getBundles() {
        return cc.assetManager.bundles;
    }
    getBundleByName(name: string) {
        return cc.assetManager.bundles.get(name);
    }
    bundleConfig: BundleConfigCollection = null;
    remotePreloadConfigCol: Col<PreloadConfig>
    //载入
    async init(config: BundleConfigCollection, remotePreloadConfigCol: Col<PreloadConfig>) {
        if (this.state !== LoaderState.Idle) {
            console.warn("[Loader] init 失败，Loader正忙");
            return;
        }

        this.emit("load-start");

        this.emit("log", "准备远程资源列表");
        this.remotePreloadConfigCol = remotePreloadConfigCol;

        this.emit("log", "加载main预设资源");
        this.remap();
        this.emit("log", "完成加载main预设");

        this.emit("log", "加载Bundle包");
        this.state = LoaderState.LoadBundle;
        this.bundleConfig = config;
        //排列载入顺序
        let loadList = [];
        for (let bundleName in this.bundleConfig) {
            loadList.push(this.bundleConfig[bundleName]);
        }
        loadList.sort((a, b) => {
            return a.index - b.index;
        });
        for (let i = 0; i < loadList.length; i++) {
            try {
                await this.loadOneBundle(loadList[i]);
                this.emit("log", `载入bundle包完成：${loadList[i].name}`);
            }
            catch (e) {
                this.emit("log", `载入bundle包失败：${loadList[i].name}`);
            }
        }
        this.emit("log", "加载Bundle包完成");
        this.state = LoaderState.LoadBundleOk;

    }
    async preload() {
        if (this.state !== LoaderState.LoadBundleOk) {
            console.warn("[Loader] preload 失败，还没载入bundle包");
            return;
        }
        this.emit("preload-assets-start");
        this.state = LoaderState.PreloadAsset;
        this.pipes = [];
        this.genPipeBundle();
        this.genPipeRemote();
        this.emit("log", "开始PreloadAsset：");
        await this.preloadPipes();
        this.state = LoaderState.Idle;
        this.emit("log", "载入完成");
        this.emit("load-end");
    }
    pipes: LoadPipe[] = [];
    async preloadPipes() {
        let totalPreloadCount = this.pipes.length;
        let curPreloadCount = 0;
        //开始预载
        NativeLogger.log(`-----开始预载 ${totalPreloadCount} 个资源`);
        this.emit("log", "预加载资源");
        this.emit("progress", 0);
        let pms = [];
        for (let i = 0; i < totalPreloadCount; i++) {
            let pipe = this.pipes[i];
            pipe.on("fail", (err) => {
                NativeLogger.log("preload,fail", pipe.config.path, err);
                curPreloadCount++;
            }, this);
            pipe.on("success", (ah: AssetHandle) => {
                this.setAsset(ah);
                NativeLogger.log("preload,ok", ah.path, ah.group, pipe.config.type);
                this.emit("log", `预加载资源 ${ah.name}(${ah.path})`);
                this.emit("progress", curPreloadCount / totalPreloadCount);
                curPreloadCount++;
            }, this);
            pms.push(pipe.load());
        }
        await Promise.all(pms);
        NativeLogger.log(`-----预载完毕`);
    }
    async genPipeRemote() {
        //
        for (let key in this.remotePreloadConfigCol) {
            let config = this.remotePreloadConfigCol[key];
            let pipe = new LoadPipeRemote();
            pipe.assetName = key;
            pipe.config = config;
            this.pipes.push(pipe);
        }
    }
    async genPipeBundle() {
        let bundleConfig: BundleConfigCollection = this.bundleConfig;

        //收集所有需要preload的bundle与路径。
        for (let bundleName in bundleConfig) {
            let bc = bundleConfig[bundleName];
            let bundle = cc.assetManager.bundles.get(bc.name);
            if (!bundle) {
                continue;
            }

            //默认的预载
            DefaultLoad.forEach(pc => {
                let ctor = cc.js.getClassByName(pc.type);
                bundle.getDirWithPath(pc.path).forEach(info => {
                    if (!ctor || ctor == info.ctor) {
                        let pipe = new LoadPipeBundle();
                        pipe.bundle = bundle;
                        pipe.config = new PreloadConfig(info.path, pc.group, pc.type);
                        this.pipes.push(pipe);
                    }
                });
            });

            //配置写的预载。
            for (let i = 0; i < bc.preload.length; i++) {
                let pc: PreloadConfig = bc.preload[i];
                if (bundle) {
                    if (pc.path[pc.path.length - 1] == "/") {
                        // {"uuid":"563e6db7-2240-46e5-afd8-27fe017fc4a8","path":"unuse/scene-2","redirect":"main"},
                        // {"uuid":"612690d2-70e0-4dc4-aa42-718152ef26e9","path":"unuse/briefing-2","redirect":"main"},
                        // {"uuid":"9dc1e338-d5ac-419a-8c25-8d79548c5fba","path":"unuse/scene-bak"}
                        bundle.getDirWithPath(pc.path).forEach(info => {
                            let pipe = new LoadPipeBundle();
                            pipe.bundle = bundle;
                            pipe.config = new PreloadConfig(info.path, pc.group, pc.type);
                            this.pipes.push(pipe);
                        });
                    }
                    else {
                        let pipe = new LoadPipeBundle();
                        pipe.bundle = bundle;
                        pipe.config = pc;
                        this.pipes.push(pipe);
                    }
                }
            }
        }
    }

    // setAssetWithBundle(ah: AssetHandle, bundle: cc.AssetManager.Bundle) {
    //     this.setAsset(ah);
    //     if (!this._bundleAssetNameMap[bundle.name]) {
    //         this._bundleAssetNameMap[bundle.name] = {};
    //     }
    //     if (!this._bundleAssetPathMap[bundle.name]) {
    //         this._bundleAssetPathMap[bundle.name] = {};
    //     }

    //     this._bundleAssetNameMap[bundle.name][ah.name] = ah;
    //     this._bundleAssetPathMap[bundle.name][ah.path] = ah;

    //     return ah;
    // }

    //todo: cc.SpriteFrame 资源会被识别为 cc.Texture2D
    //todo: 动态加载资源流程 需要优化
    loadAsset<T extends cc.Asset>(assetPathWithMime: string, group: string = "", className: string = "", targetAssetName = "") {
        return new Promise<T>(ok => {
            let assetPath = assetPathWithMime.slice(0, assetPathWithMime.lastIndexOf("."));
            let assetName = targetAssetName || assetPath.slice(assetPath.lastIndexOf("/") + 1);
            let asset = this.getAsset(assetName, group, className);
            if (asset) {
                ok(<T>asset);
                return;
            }
            let type: typeof cc.Asset = <any>cc.js.getClassByName(className);
            let bundle = this.findBundleByPath(assetPath, type);
            if (bundle) {
                bundle.load(assetPath, cc.Asset, (err, asset: cc.Asset) => {
                    if (err) {
                        NativeLogger.log("Loader::loadAsset,fail", assetPath, err);
                        ok(null);
                        return;
                    }
                    let ah = new AssetHandle();
                    ah.asset = asset;
                    ah.name = assetName;
                    ah.path = assetPath;
                    ah.group = group;
                    this.setAsset(ah);
                    ok(<T>asset);
                });
            }
        });
    }

    async loadRemote<T extends cc.Asset>(url: string, cls: new () => T, group: string, assetName: string) {
        let ah: AssetHandle = null;
        let pipe = new LoadPipeRemote();
        pipe.assetName = assetName;
        pipe.once("fail", (err) => {
            NativeLogger.log("Loader::loadRemote error:", err);
            console.error("Loader::loadRemote error:", err);
        }, this);
        pipe.once("success", (asset: T) => {
            ah = new AssetHandle();
            ah.asset = asset;
            ah.name = assetName || asset.name;
            ah.path = url;
            ah.group = group;
            this.setAsset(ah);
        }, this);

        await pipe.load();

        return <T>ah.asset;
    }

    /** 寻找含有XX资源的Bundle */
    public findBundleByPath(path: string, type: typeof cc.Asset) {
        if (!path) {
            cc.error("路径传入错误:", path);
            return null;
        }
        for (let i = this.bundles.length - 1; i >= 0; i--) {
            let bundle = this.bundles[i];
            let info = bundle.getInfoWithPath(path, type);
            if (info) {
                return bundle;
            }
        }
        cc.error("所有包中都没有找到此资源:", path);
        return null;
    }
    public findBundleByPathArray(path: string, type: typeof cc.Asset) {
        if (!path) {
            cc.error("路径传入错误:", path);
            return null;
        }
        let bundleArray = [];
        for (let i = this.bundles.length - 1; i >= 0; i--) {
            let bundle = this.bundles[i];
            let info = bundle.getInfoWithPath(path, type);
            if (info) {
                bundleArray.push(bundle);
            }
        }
        if (bundleArray.length == 0) {
            cc.error("所有包中都没有找到此资源:", path);
        }
        return bundleArray;
    }

    async restoreOneBundle(info: BundleInfo) {
        if (info.isLoaded) {
            cc.assetManager.removeBundle(cc.assetManager.bundles.get(info.config.name));
            info.isLoaded = false;
        }
        Utils.jsb_removeDir(info.abPath);
        let config: BundleConfig = Utils.loader.originVersionJson.json[info.config.name];
        info.config = config;
        info.from = "origin";
        let vc = Utils.loader.loadVersionLocal();
        vc[info.config.name] = Utils.loader.originVersionJson.json[info.config.name];
        Utils.loader.saveVersionLocal(vc);

        await Utils.loader.loadOneBundle(info.config);
    }

    loadOneBundle(bundleConfig: BundleConfig) {
        return new Promise<void>((ok, fail) => {
            let loadInfo = new BundleInfo();
            loadInfo.config = bundleConfig;
            let originConf: BundleConfig = this.originVersionJson.json[bundleConfig.name];
            let hasOrigin = !!originConf;
            let hasLocal = false;

            if (cc.sys.isNative) {
                let writePath = jsb.fileUtils.getWritablePath();
                let bundlePath = `${writePath}/Bundles`;

                loadInfo.abPath = `${bundlePath}/${bundleConfig.name}/`;
                //NATIVE 检查是否有包缓存。
                if (jsb.fileUtils.isDirectoryExist(`${bundlePath}/${bundleConfig.name}`)) {
                    hasLocal = true;
                    loadInfo.from = "local";
                }

                if (!hasLocal) {
                    let lc = this.loadVersionLocal();
                    if (hasOrigin) {
                        lc[bundleConfig.name] = originConf;
                        loadInfo.config = originConf;
                        loadInfo.from = "origin";
                    }
                    else {
                        if (lc[bundleConfig.name]) {
                            delete lc[bundleConfig.name];
                        }
                    }
                    Utils.loader.saveVersionLocal(lc);
                }

                if (hasLocal) {
                    NativeLogger.log(`载入bundle包${bundleConfig.name}，本地缓存`);
                    data.bundleInfos.push(loadInfo);
                    cc.assetManager.loadBundle(`${bundlePath}/${bundleConfig.name}`, (err, bundle) => {
                        if (err) {
                            fail(err);
                            return;
                        }
                        this.bundles.push(bundle);
                        loadInfo.isLoaded = true;
                        ok();
                    });
                }
                else if (hasOrigin) {
                    NativeLogger.log(`载入bundle包${bundleConfig.name}，从APP`);
                    loadInfo.from = "origin";
                    data.bundleInfos.push(loadInfo);
                    cc.assetManager.loadBundle(`assets/${bundleConfig.name}`, (err, bundle: cc.AssetManager.Bundle) => {
                        if (err) {
                            fail(err);
                            return;
                        }
                        this.bundles.push(bundle);
                        loadInfo.isLoaded = true;
                        ok();
                    });
                }
                else {
                    ok();
                }
            }
            else {
                loadInfo.abPath = "";
                loadInfo.from = "web";
                data.bundleInfos.push(loadInfo);
                let bundlePath = "";
                if (loadInfo.config.url) {
                    bundlePath = `${loadInfo.config.url}/${bundleConfig.name}`;
                }
                else {
                    bundlePath = `assets/${bundleConfig.name}`;
                }
                cc.assetManager.loadBundle(bundlePath, (err, bundle: cc.AssetManager.Bundle) => {
                    if (err) {
                        fail(err);
                        return;
                    }
                    this.bundles.push(bundle);
                    loadInfo.isLoaded = true;
                    ok();
                });
            }
        });
    }


    loadVersionLocal() {
        let versionLocal: BundleConfigCollection = <BundleConfigCollection>this.originVersionJson.json;
        try {
            if (cc.sys.isNative) {
                if (window.localStorage.getItem(`${Macro.APP_NAME}/version`)) {
                    versionLocal = <BundleConfigCollection>JSON.parse(window.localStorage.getItem(`${Macro.APP_NAME}/version`))
                }
            }
        } catch (e) {
        }

        return versionLocal;
    }
    saveVersionLocal(versionLocal: BundleConfigCollection) {
        //保存version
        window.localStorage.setItem(`${Macro.APP_NAME}/version`, JSON.stringify(versionLocal));
    }
    // 检查bundle包更新
    checkUpdate() {
        return new Promise<CheckUpdateResult>(async (ok, fail) => {
            if (!cc.sys.isNative) {
                fail("非Native，无需更新");
                return;
            }

            if (this.state !== LoaderState.Idle) {
                fail("Loader正忙，不能checkUpdate");
                return;
            }
            this.state = LoaderState.CheckUpdate;

            let versionLocal: BundleConfigCollection = this.loadVersionLocal();
            NativeLogger.log(`开始检查更新：${Http.getRemoveVersionPath()}`);
            NativeLogger.log("APP版本：", this.originVersionJson.json);
            NativeLogger.log("本地版本：", versionLocal);

            let versionRemote: BundleConfigCollection = null;
            try {
                versionRemote = await new Promise<any>(ok => {
                    cc.assetManager.loadRemote(`${Http.getRemoveVersionPath()}`, null, (err, data: any) => {
                        if (err) {
                            return null;
                        }
                        ok(<BundleConfigCollection>data.json);
                    });
                });
            } catch (e) {
                NativeLogger.log(`获取version失败，${e}`);
                fail(`获取version失败，${e}`);
                this.state = LoaderState.Idle;
                return;
            }

            NativeLogger.log("远程版本：", versionRemote);


            NativeLogger.log(`local <-> remote`);
            let needUpdateList = [];
            for (let key in versionRemote) {
                let bvRemote = versionRemote[key];

                if (0/**Number(bvRemote.appVersion) > Number(Macro.APP_VERSION)**/) {
                    let bvLocal = versionLocal[key];
                    if (!bvLocal) {
                        needUpdateList.push(key);
                        NativeLogger.log(`新包：${bvRemote.name}:${bvRemote.version}`);
                    }
                    else {
                        if (Number(bvLocal.version) < Number(bvRemote.version)) {
                            needUpdateList.push(key);
                            NativeLogger.log(`更新包：${bvLocal.name}:${bvLocal.version} <-> ${bvRemote.name}:${bvRemote.version}`);
                        }
                        else {
                            NativeLogger.log(`已有包：${bvLocal.name}:${bvLocal.version} <-> ${bvRemote.name}:${bvRemote.version}`);
                        }
                    }
                }
                else {
                    NativeLogger.log(`已有包：${bvRemote.name}:${bvRemote.version}`);
                }
            }
            NativeLogger.log("需要更新的bundle列表:", needUpdateList);

            this.state = LoaderState.Idle;
            ok(<CheckUpdateResult>{
                versionRemote,
                versionLocal,
                needUpdateList
            });
        });
    }
    async updateBundle(result: CheckUpdateResult) {
        let totalCount = result.needUpdateList.length;
        this.emit("update-start");
        this.emit("log", "开始更新Bundle包");
        this.emit("progress", 0);
        let finishCount = 0;
        for (let i = 0; i < result.needUpdateList.length; i++) {
            let bundleName = result.needUpdateList[i];
            this.emit("log", `更新${bundleName} (${finishCount}/${totalCount})`);
            try {
                await this.updateOneBundle(bundleName);
            } catch (err) {
                NativeLogger.log("Bundle加载失败", err);
            }
            finishCount++;
            result.versionLocal[bundleName] = result.versionRemote[bundleName];

            this.emit("progress", finishCount / totalCount);
        }
        this.emit("log", "更新完成");
        this.emit("update-end");
        NativeLogger.log("----更新完成----");
        //保存version
        window.localStorage.setItem(`${Macro.APP_NAME}/version`, JSON.stringify(result.versionLocal));
    }

    //writePath : 可写路径
    //bundlePath: bundle包总路径  writePath/Bundles/
    //remotePath: bundle包远程路径 update_service/Bundles/xxxxx
    //targetPath: bundle包目标本地路径 writePath/Bundles/xxxxx
    //cachePath:  zip在临时文件夹的路径 writePath/gamecaches/
    async updateOneBundle(bundleName: string) {
        let writePath = jsb.fileUtils.getWritablePath();
        let bundlePath = `${writePath}/Bundles`;
        if (!jsb.fileUtils.isDirectoryExist(bundlePath)) {
            jsb.fileUtils.createDirectory(bundlePath);
        }

        let remotePath = `${Http.getRemoteBundlePath()}/${bundleName}.zip`;
        let targetPath = `${writePath}/Bundles/${bundleName}`;
        NativeLogger.log(`开始更新Bundle包: ${bundleName}, 地址是: ${remotePath}`);
        return new Promise<void>((ok, reject) => {
            cc.assetManager.loadAny({ url: remotePath }, null, async (err: Error, binaryData: any) => {
                if (err) {
                    NativeLogger.log(`Bundle包下载失败: ${err},${bundleName}`);
                    NativeLogger.log(err);
                    reject(`Bundle包下载失败: ${err},${bundleName}`);
                    return null;
                }

                NativeLogger.log(`Bundle包下载成功: ${bundleName}`);

                let cachePath = cc.assetManager.cacheManager.getCache(remotePath);
                let data = jsb.fileUtils["getDataFromFile"](cachePath);
                //@ts-ignore
                let newZip = new JSZip();
                let zip = await newZip.loadAsync(data);
                if (!zip) {
                    reject(`Zip包读取失败`);
                    return;
                }
                NativeLogger.log(`解压zip包到${targetPath}`);

                let objPathArr = [];
                zip.forEach((path, obj) => {
                    objPathArr.push([path, obj]);
                });

                for (let i = 0; i < objPathArr.length; i++) {
                    let [path, obj] = objPathArr[i];
                    if (!zip.file(path) || zip.file(path).dir) {
                        continue;
                    }
                    await new Promise<void>(async (unzipOk, unzipFail) => {
                        let fileData = await obj.async("uint8array");

                        //文件夹。
                        let dirs = path.split("/");
                        dirs = dirs.slice(0, dirs.length - 1);
                        let checkPath = bundlePath;
                        for (let i = 0; i < dirs.length; i++) {
                            checkPath += "/" + dirs[i];
                            if (!jsb.fileUtils.isDirectoryExist(checkPath)) {
                                jsb.fileUtils.createDirectory(checkPath);
                            }
                        }

                        if (jsb.fileUtils["writeDataToFile"](fileData, `${bundlePath}/${path}`)) {
                            NativeLogger.log(`解压文件成功：${bundlePath}/${path}`);
                            unzipOk();
                        }
                        else {
                            NativeLogger.log(`解压文件失败：${bundlePath}/${path}`);
                            unzipFail();
                        }
                    });
                }
                ok();
            });
        });
    }
};