import { SubjectComponent } from "../../../core/subject";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Selecter2 extends SubjectComponent {
    @property(cc.Label)
    lb: cc.Label = null;

    @property
    circle = false;

    @property(cc.Node)
    btnLeft: cc.Node = null;

    @property(cc.Node)
    btnRight: cc.Node = null;

    @property
    curInd = 0;
    get curSelect() {
        return this.selects[this.curInd];
    }
    @property([cc.String])
    selects: string[] = [];

    onLoad() {
        this.btnLeft.on(cc.Node.EventType.TOUCH_END, this.onClickPrev, this);
        this.btnRight.on(cc.Node.EventType.TOUCH_END, this.onClickNext, this);
    }

    onClickPrev() {
        let ind = this.curInd - 1;
        if (ind < 0) {
            if (this.circle) {
                ind = this.selects.length - 1;
            }
            else {
                return;
            }
        }
        this.sel(ind);
    }
    onClickNext() {
        let ind = this.curInd + 1;
        if (ind > this.selects.length - 1) {
            if (this.circle) {
                ind = 0;
            }
            else {
                return;
            }
        }
        this.sel(ind);
    }

    sel(ind: number) {
        if (this.curInd === ind) {
            return;
        }
        this.curInd = ind;
        this.lb.string = this.selects[this.curInd];
        this.emit("changed", this.curInd);
    }
};