import { SubjectComponent } from "../../core/subject";

const { ccclass, property } = cc._decorator;

export enum SubBodyEventType {
    None = 0,
    Begin = 1,
    End,
    PreSolve,
    PostSolve,
};

@ccclass("SubBodyEventContact")
export class SubBodyEventContact {
    type: SubBodyEventType = SubBodyEventType.None;
    contact: cc.PhysicsContact = null;
    self: cc.PhysicsCollider = null;
    other: cc.PhysicsCollider = null;
};


//可以用作转发碰撞事件
@ccclass
export default class SubBody extends SubjectComponent {
    protected _body: cc.RigidBody = null;
    @property(cc.RigidBody)
    get body() {
        if (!this._body) {
            this._body = this.getComponent(cc.RigidBody) || this.addComponent(cc.RigidBody);
        }
        return this._body;
    }
    protected onLoad(): void {
        super.onLoad && super.onLoad();
        this.body.enabledContactListener = true;
    }


    onBeginContact(contact: cc.PhysicsContact, self: cc.PhysicsCollider, other: cc.PhysicsCollider) {
        let evt = new SubBodyEventContact();
        evt.self = self;
        evt.other = other;
        evt.contact = contact;
        evt.type = SubBodyEventType.Begin;
        this.emit(SubBodyEventContact, evt);
    }
    onEndContact(contact: cc.PhysicsContact, self: cc.PhysicsCollider, other: cc.PhysicsCollider) {
        let evt = new SubBodyEventContact();
        evt.self = self;
        evt.other = other;
        evt.contact = contact;
        evt.type = SubBodyEventType.End;
        this.emit(SubBodyEventContact, evt);
    }
    onPreSolve(contact: cc.PhysicsContact, self: cc.PhysicsCollider, other: cc.PhysicsCollider) {
        let evt = new SubBodyEventContact();
        evt.self = self;
        evt.other = other;
        evt.contact = contact;
        evt.type = SubBodyEventType.PreSolve;
        this.emit(SubBodyEventContact, evt);
    }
    onPostSolve(contact: cc.PhysicsContact, self: cc.PhysicsCollider, other: cc.PhysicsCollider) {
        let evt = new SubBodyEventContact();
        evt.self = self;
        evt.other = other;
        evt.contact = contact;
        evt.type = SubBodyEventType.PostSolve;
        this.emit(SubBodyEventContact, evt);
    }
};