import ShaderBase from "../shader_base";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ShaderOpacity extends ShaderBase {
    @property({
        visible: false,
        serializable: true
    })
    _opacity = 0;

    @property({
        slide: true,
        min: 0,
        max: 1,
        step: 0.1,
    })
    get opacity() {
        return this._opacity;
    }

    set opacity(val) {
        this._opacity = val;
        this.refresh();
    }

    refresh(): void {
        this.material.setProperty("opacity", this._opacity);
    }
}
