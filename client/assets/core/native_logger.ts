import Macro from "./macro";

export default class NativeLogger {
    static fileName: string = "";
    static abPath: string = "";
    static init() {
        if (!cc.sys.isNative) {
            return;
        }
        if (Macro.BUILD_TARGET !== "test") {
            return;
        }
        this.abPath = `${jsb.fileUtils.getWritablePath()}/DebugLog`;
        this.fileName = "output"; //Date.now().toString();
        if (!jsb.fileUtils.isDirectoryExist(this.abPath)) {
            jsb.fileUtils.createDirectory(this.abPath);
        }
        this.__writeFileInc(this.fileName);
    }
    static log(...args: any[]) {
        if (!cc.sys.isNative) {
            return;
        }
        if (Macro.BUILD_TARGET !== "test") {
            return;
        }
        // cc.log(args);
        let data = "";
        try {
            let strs = [];
            args.forEach(arg => {
                strs.push(JSON.stringify(arg));
            })
            data = data.concat(strs.join(", "));

            this.__writeFileInc(data);
        } catch (e) { }
    }

    private static __writeFileInc(incStr: string) {
        let srcStr = jsb.fileUtils.getStringFromFile(`${this.abPath}/${this.fileName}`);
        jsb.fileUtils.writeStringToFile(`${srcStr}\n${incStr}`, `${this.abPath}/${this.fileName}`);
    }

    private static __writeFile(incStr: string) {
        jsb.fileUtils.writeStringToFile(`${incStr}`, `${this.abPath}/${this.fileName}`);
    }

    static propertyOf(obj: any) {
        let ps = [];
        for (let key in obj) {
            ps.push(key);
        }
        return ps;
    }
};