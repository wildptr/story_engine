import data from "../core/cache_data";
import GameTask from "../core/game_task";
import Macro from "../core/macro";
import Utils from "../core/utils";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameTaskData extends GameTask {
    async init() {
        data.load();
        this.onFinished();
    }
};  