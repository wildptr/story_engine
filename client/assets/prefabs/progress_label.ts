import Progress from "./progress";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ProgressLabel extends Progress {
    @property(cc.Label)
    label: cc.Label = null;
    @property()
    format = "{{val}}%";
    @property()
    fixed = 2;
    @property()
    mul = 100;

    refresh(): void {
        this.label.string = this.format.replace("\{\{val\}\}", `${(this.progress * this.mul).toFixed(this.fixed)}`);
    }
};