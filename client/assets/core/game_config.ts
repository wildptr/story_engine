import { HttpConfig } from "./http_request";
import { BundleConfigCollection } from "./loader";
import SerializeAble, { SerializeClass, Serialize } from "./serialize";

@SerializeClass("WeiXinConfig")
export class WeiXinConfig extends SerializeAble {
    @Serialize()
    enabled = false;
    @Serialize()
    shareTitle = "分享标题";
    @Serialize()
    shareDesc = "分享详细文字内容";
    @Serialize()
    shareImgUrl = "";

    @Serialize()
    getSignUrl = ""; //获取签名的请求URL。
    @Serialize()
    sdkVersion = "1.3.2";//微信SDK版本。
};


@SerializeClass("GameLoadingConfig")
export class GameLoadingConfig extends SerializeAble {
    @Serialize()
    boxLoadingName = "BoxLoading"; // 引擎载入后的LOADING
    @Serialize()
    bgUrl = "";
    @Serialize()
    bgInAsset = "";
    @Serialize()
    hasLabel = true;
    @Serialize()
    hasIcon = true;
};

@SerializeClass("GameConfig")
export class GameConfig extends SerializeAble {
    @Serialize()
    key = ""; //游戏唯一名字 英文字母 PingPongGame / ping_pong_game
    @Serialize()
    name = ""; //游戏名字
    @Serialize()
    desc = ""; //游戏描述
    @Serialize()
    version = ""; //游戏版本 
    @Serialize()
    timestamp = ""; //游戏版本时间戳
    @Serialize()
    screen: "Landscape" | "Vertical";
    @Serialize()
    defaultStartPage = "";//开始页面
    @Serialize()
    bundleConfigCol: BundleConfigCollection = {}; //BUNDLE包内容
    @Serialize(WeiXinConfig)
    wxConfig: WeiXinConfig = new WeiXinConfig();
    @Serialize()
    gameTaskList: string[] = [];
    @Serialize(HttpConfig)
    httpConfig: HttpConfig = new HttpConfig();
    @Serialize(GameLoadingConfig)
    loading: GameLoadingConfig = new GameLoadingConfig(); // 载入画面，如果为空则用默认
};
