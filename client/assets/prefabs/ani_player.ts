import Utils from "../core/utils";
const { ccclass, property } = cc._decorator;

@ccclass
export default class AniPlayer extends cc.Component {
    @property
    playOnStart = true;
    @property({
        visible() {
            return this.playOnStart;
        },
    })
    delay = 0;

    @property({
        visible() {
            return this.playOnStart;
        },
    })
    playAniName = "";

    _ani: cc.Animation = null;
    @property(cc.Animation)
    get ani() {
        if (!this._ani) {
            this._ani = this.getComponent(cc.Animation);
        }
        return this._ani
    }

    onLoad() {
        if (!this.ani) {
            cc.log("AniPlayer: cant find cc.Animation");
            this.destroy();
            return;
        }
    }

    async start() {
        if (this.playOnStart) {
            if (this.delay > 0) {
                await new Promise<void>(ok => {
                    cc.tween(this.node).delay(this.delay).call(ok).start();
                });
            }
            this.ani.play(this.playAniName);
        }
    }
    aniDestroy() {
        this.node.destroy();
    }

    emitAniMsg(msg) {
        Utils.msgHub.emit(Utils.macro.EVENTS.ANI_MSG, msg);
    }
    playClip(name) {
        SoundManager.ins.playClip(name);
    }
    playClipEx(name, loop, volDetail, isFade) {
        SoundManager.ins.playClip(name, loop, volDetail);
    }
    stopClip(name) {
        SoundManager.ins.stopClip(name);
    }
    stopClipEx(name, isFade) {
        SoundManager.ins.stopClip(name);
    }
    playAni(name, additive) {
        try {
            var ani = this.getComponent(cc.Animation);
            if (additive) {
                ani.playAdditive(name);
            }
            else {
                ani.play(name);
            }
        } catch (e) { }
    }
}
