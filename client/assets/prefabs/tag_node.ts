const { ccclass, property } = cc._decorator;

@ccclass
export default class TagNode extends cc.Component {
    @property
    tag = "";
    static GetTagNodeByTag(node: cc.Node, tag: string) {
        let arr: TagNode[] = [];
        node.getComponentsInChildren(TagNode).forEach(tn => {
            if (tn.tag == tag) {
                arr.push(tn);
            }
        });
        return arr;
    }
};