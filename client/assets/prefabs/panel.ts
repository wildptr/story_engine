import { SubjectComponent } from "../core/subject";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Panel extends SubjectComponent {
    _blocked = true;
    onDestroy() {
        this.emit("destroy", this);
    }
    onLoad() {
        let panel = cc.find("panel", this.node);
        let sc = cc.find("sc", this.node);
        let ani = this.getComponent(cc.Animation);
        if (panel && ani && sc) {
            sc.opacity = 0;
            panel.opacity = 0;
            ani.play();
        }
    }
    start() {
        cc.tween(this.node).delay(.5).call(() => {
            this._blocked = false;
        }).start();
    }

    onClickClose() {
        if (this._blocked) {
            return;
        }
        this._blocked = true;
        this.node.destroy();
    }
};