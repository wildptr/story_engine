const { ccclass, property } = cc._decorator;

/** 键盘回调节点 */
@ccclass
export default class KeyboardNode extends cc.Component {
    protected onLoad(): void {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    }
    protected onDestroy(): void {
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    }
    onKeyDown(evt: cc.Event.EventKeyboard) {
        this.onKey && this.onKey(evt.keyCode);
        switch (evt.keyCode) {
            case cc.macro.KEY.w:
                this.onKeyW && this.onKeyW();
                break;
            case cc.macro.KEY.s:
                this.onKeyS && this.onKeyS();
                break;
            case cc.macro.KEY.a:
                this.onKeyA && this.onKeyA();
                break;
            case cc.macro.KEY.d:
                this.onKeyD && this.onKeyD();
                break;
        }
        this.onLog && this.onLog();
    }
    onKey: (keyCode: cc.macro.KEY) => void = null;
    onKeyW: () => void = null;
    onKeyS: () => void = null;
    onKeyA: () => void = null;
    onKeyD: () => void = null;
    onLog: () => void = null;
};