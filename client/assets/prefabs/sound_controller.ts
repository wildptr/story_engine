import SoundManager from "../core/sound_manager";
import { SubjectComponent } from "../core/subject";
import Utils from "../core/utils";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SoundController extends SubjectComponent {
    @property(cc.Sprite)
    face: cc.Sprite = null;
    @property([cc.SpriteFrame])
    frames: cc.SpriteFrame[] = [];

    start() {
        this.refresh();
    }

    refresh() {
        if (SoundManager.ins.mute) {
            this.face.spriteFrame = this.frames[0];
        }
        else {
            this.face.spriteFrame = this.frames[1];
        }
    }
    onClickSound() {
        SoundManager.ins.toggleMute();
        this.refresh();
    }
}