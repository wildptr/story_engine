const { ccclass, property } = cc._decorator;

@ccclass
export default abstract class ShaderBase extends cc.Component {
    _rc: cc.RenderComponent = null;
    get sprite() {
        if (!this._rc) {
            let rc = this.getComponent(cc.RenderComponent);
            if (!rc) {
                rc = this.addComponent(cc.RenderComponent);
            }
            if (!rc) {
                cc.error("ShaderBase::sprite, error: 不能获得或添加cc.RenderComponent组件。");
                return;
            }
            this._rc = rc;
        }
        return this._rc;
    }

    _material: cc.Material = null;
    get material() {
        return this.sprite.getMaterial(0);
    }

    abstract refresh(): void;

    protected onLoad(): void {
        this.refresh();
    }
};