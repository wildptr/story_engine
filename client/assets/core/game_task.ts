import { SubjectComponent } from "./subject";

const { ccclass, property } = cc._decorator;

@ccclass
export default abstract class GameTask extends SubjectComponent {
    @property
    isSync = false;
    @property
    isBeforeGame = true;

    isDone = false;
    abstract init();
    onFinished() {
        this.isDone = true;
        this.emit("finished", this);
    }
};