
//遍历文件夹文件并压缩成JSZIP
import fs from 'fs'
import { execSync } from 'child_process';

(async () => {
    let _read = async (path) => {
        const fsDir = fs.opendirSync(path);
        let childDirent = fsDir.readSync();
        while (childDirent) {
            if (childDirent.isDirectory()) {
                await _read(path + childDirent.name + "/");
            }
            else {
                if (childDirent.name.split(".")[1] == "png") {
                    await execSync(`pngquant -f --ext .png --quality 10-90 --speed 1 "${path + childDirent.name}"`);
                    console.log(`pngquant: 压缩图片"${path + childDirent.name}"`);
                }
            }
            childDirent = fsDir.readSync();
        }
        fsDir.closeSync();
    };

    await _read("./build/web-mobile/");
})();