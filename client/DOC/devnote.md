# 构建
- 环境
    - 安装 nodejs 环境
    - npm install 
- 构建流程
    - COCOS构建项目
    - 根据需求配置 buildBundleGame.json
    - 在项目目录中执行node buildBundleGame.mjs [构建目标test/res] 
- 框架构建流程
    - COCOS构建项目
    - 根据需求配置 buildFrame.json
    - 在项目目录中执行node buildFrame.mjs [构建目标test/res] 
- Bundle构建流程
    - COCOS构建项目
    - 在项目目录中执行node buildBundle.mjs [bundle包名1] [bundle包名2] [bundle包名3] [...]

- 发布流程
    - 先按上面的流程构建，然后执行publish.bat
    - 使用sftp 同步到服务器。有框架和bundles 两种。

# 发音网站
https://www.text-to-speech.cn/

# cojoy 服务器
39.96.8.172
aMZ9RmKPNuzZQ
39.106.3.252
EgKv9Pp2dRkYlLbY

# BUNDLE包：
- msgjug_statistics 加入了statistics包，MSGJUG的统计
    - 包构建流程
        - 先构建整个项目
        - 执行 node buildBundle.mjs [target] [包名....]