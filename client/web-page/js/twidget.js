var FontMelement = function (dom, fontSize, fontSizeUnit) {
    this.dom = dom;
    this.fontSize = fontSize;

    this.fontSizeUnit = fontSizeUnit || "px";
};

FontMelement.prototype.update = function (ph, origin, size) {
    if (this.fontSize != null) {
        this.dom.style.fontSize = this.fontSize * ph + this.fontSizeUnit;
    }
};

var CMelement = function (dom, x, y, width, height, xUnit, yUnit, widthUnit, heightUnit) {
    this.dom = dom;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;

    this.xUnit = xUnit || "px";
    this.yUnit = yUnit || "px";
    this.widthUnit = widthUnit || "px";
    this.heightUnit = heightUnit || "px";
    this.dom.melement = this;
};
CMelement.prototype.update = function (ph, origin, size) {
    if (this.x != null) {
        var width = this.width || this.dom.clientWidth;
        this.dom.style.left = (this.x - width / 2) * ph + origin.x + this.xUnit;
    }
    if (this.y != null) {
        var height = this.height || this.dom.clientHeight;
        this.dom.style.top = (this.y - height / 2) * ph + origin.y + this.yUnit;
    }
    if (this.width != null) {
        this.dom.style.width = this.width * ph + this.widthUnit;
    }
    if (this.height != null) {
        this.dom.style.height = this.height * ph + this.heightUnit;
    }
};
var Melement = function (dom, left, top, width, height, leftUnit, topUnit, widthUnit, heightUnit) {
    this.dom = dom;
    this.left = left;
    this.top = top;
    this.width = width;
    this.height = height;

    this.leftUnit = leftUnit || "px";
    this.topUnit = topUnit || "px";
    this.widthUnit = widthUnit || "px";
    this.heightUnit = heightUnit || "px";
};

Melement.prototype.update = function (ph) {
    if (this.left != null) {
        this.dom.style.left = this.left * ph + this.leftUnit;
    }
    if (this.top != null) {
        this.dom.style.top = this.top * ph + this.topUnit;
    }
    if (this.width != null) {
        this.dom.style.width = this.width * ph + this.widthUnit;
    }
    if (this.height != null) {
        this.dom.style.height = this.height * ph + this.heightUnit;
    }
};

var ArtWord = function (picSrc, twidget, splash, digit, cellW, cellH) {
    this.mes = [];
    Object.assign(this, {
        picSrc, twidget, splash, digit, cellW, cellH
    });
    this.dom = document.createElement("div");
    Object.assign(this.dom.style, {
        pointerEvents: "none",
        position: "absolute",
    });
    this.splash.appendChild(this.dom);
    this.initMe();
};
ArtWord.BG_TABLE = {
    "%": "-10%",
    1: "0%",
    2: "10%",
    3: "20%",
    4: "30%",
    5: "40%",
    6: "50%",
    7: "60%",
    8: "70%",
    9: "80%",
    0: "90%",
};
ArtWord.prototype.initMe = function () {
    var w = this.digit * this.cellW;
    var w2 = w / 2;
    var pos = 0;
    for (var i = 0; i < this.digit + 1; i++) {
        var word = document.createElement("div");
        Object.assign(word.style, {
            pointerEvents: "none",
            background: "url(" + this.picSrc + " )",
            backgroundSize: "cover",
            backgroundPosition: "0%",
            position: "absolute",
        });

        this.dom.appendChild(word);
        var me = this.twidget.addMe(new CMelement(word, -w2 + (pos * this.cellW) + this.cellW / 2, 0, this.cellW, this.cellH));
        this.mes.push(me);
        pos++;
    }
    this.mes[this.mes.length - 1].dom.style.display = "";
    this.mes[this.mes.length - 1].dom.style.backgroundPosition = ArtWord.BG_TABLE["%"];
};

ArtWord.prototype.set = function (val) {
    var str = val.toFixed(0);
    var meInd = this.mes.length - 2;
    for (let i = str.length - 1; i >= 0; i--) {
        this.mes[meInd].dom.style.display = "";
        this.mes[meInd].dom.style.backgroundPosition = ArtWord.BG_TABLE[str[i]];
        meInd--;
    }
    var realDigit = meInd + 1;

    while (meInd >= 0) {
        this.mes[meInd].dom.style.display = "none";
        meInd--;
    }

    this.refresh(realDigit);
};

ArtWord.prototype.refresh = function (realDigit) {
    // var w = realDigit * this.cellW;
    var w = this.digit * this.cellW;
    var w2 = w / 2;
    var pos = 0;
    for (let i = realDigit; i < this.mes.length; i++) {
        var me = this.mes[i];
        me.x = -w2 + (pos * this.cellW) + this.cellW / 2;
        pos++;
    }
};


var TWidget = function () {
    this.mes = [];
};

TWidget.prototype.addMe = function (me) {
    this.mes.push(me);
    return me;
};
TWidget.prototype.removeMe = function (me) {
    for (var i = 0; i < this.mes.length; i++) {
        if (me == this.mes[i]) {
            return this.mes.splice(i, 1)[0];
        }
    }
    return null;
};

TWidget.prototype.update = function (dt) {
    var size = {
        width: window.innerWidth,
        height: window.innerHeight
    };
    var origin = {
        x: size.width / 2,
        y: size.height / 2
    };
    var ph = size.width / 750;
    for (var i = 0; i < this.mes.length; i++) {
        this.mes[i].update(ph, origin, size);
    }
};