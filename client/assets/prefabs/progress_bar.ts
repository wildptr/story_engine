import Progress from "./progress";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ProgressBar extends Progress {
    @property(cc.Sprite)
    bar: cc.Sprite = null;

    refresh(): void {
        this.bar.fillRange = this.progress;
    }
};