import { SubjectComponent } from "../core/subject";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CheckBox extends SubjectComponent {
    @property(cc.Node)
    markNode: cc.Node = null;


    @property
    get checked() {
        return this.markNode?.active;
    }
    set checked(val) {
        if( this.markNode ){ 
            this.markNode.active = val;
        }
    }

    protected onLoad(): void {
        this.node.on(cc.Node.EventType.TOUCH_END, this.onClick, this);
    }

    onClick() {
        this.markNode.active = !this.markNode.active;
        this.emit("clicked", this.markNode.active);
    }
};