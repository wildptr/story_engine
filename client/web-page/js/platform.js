//UA
var u = navigator.userAgent, app = navigator.appVersion;
IS_ANDROID = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
IS_IOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
IS_WEIXIN = false;
IS_WEIBO = false;
IS_QQ = false;
var ua = navigator.userAgent.toLowerCase();//获取判断用的对象
if (ua.match(/MicroMessenger/i) == "micromessenger") {
    //在微信中打开
    IS_WEIXIN = true;
}
if (ua.match(/WeiBo/i) == "weibo") {
    //在新浪微博客户端打开
    IS_WEIBO = true;
}
if (ua.match(/QQ/i) == "qq") {
    //在QQ空间打开
    IS_QQ = true;
}

window.GetRequest = function () {
    if (typeof window.urlArg != "undefined") {
        return window.urlArg;
    }
    var url = location.search; //获取url中"?"符后的字串   
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        var strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    window.urlArg = theRequest;
    return theRequest;
};

function MyWechat(details) {
    this.ready = false;
    this.userInfoReady = false;
    this.readyCallback = null;

    const loc = window.location;
    var path = loc.pathname.substring(0, loc.pathname.lastIndexOf("/") + 1);
    this.shareImgUrl = `${loc.protocol}//${loc.hostname}${loc.port ? `:${loc.port}` : ""}${path}${"share.jpg"}`;
    this.shareDetails = details;
};

MyWechat.prototype.init = function (readyCallback) {
    console.log("MyWechat::init", "hasCallback:", !!readyCallback);
    this.readyCallback = readyCallback;
    $.ajax({
        type: 'GET',
        url: 'https://open.cojoy.com.cn/wechat/jssdk/getsign.php',
        dataType: 'json',
        data: {
            _url: window.location.href
        },
        success: function (data, textStatus, jqXHR) {
            wx.config({
                beta: true,
                debug: false,
                appId: data.appId,
                timestamp: data.timestamp,
                nonceStr: data.nonceStr,
                signature: data.signature,
                jsApiList: [
                    'onMenuShareTimeline',
                    'onMenuShareAppMessage'
                ]
            });

            wx.ready(this.onWxReady.bind(this));
        }.bind(this),
        error: this.onWxReady.bind(this)
    });
};
MyWechat.prototype.onWxReady = function (readyCallback) {
    this.ready = true;
    console.log("MyWechat::onWxReady");
    this.readyCallback && this.readyCallback();
};

MyWechat.prototype.shareConfig = function (detailInd = 0) {
    console.log("MyWechat::shareConfig", detailInd);
    var ind = detailInd;
    if (detailInd == -1) {
        ind = Math.floor(Math.random() * this.shareDetails.length)
    }
    var td = this.shareDetails[ind];
    var shareInfoAppMessage = {
        title: td.title,
        desc: td.desc,
        link: window.location.href,
        imgUrl: td.shareImgUrl || this.shareImgUrl,
        type: '',
        dataUrl: '',
        success: this.onShareFinish.bind(this),
        cancel: this.onShareFinish.bind(this)
    };
    wx.onMenuShareAppMessage(shareInfoAppMessage);

    var shareInfoTimeLine = {
        title: td.title + td.desc,
        link: window.location.href,
        imgUrl: td.shareImgUrl || this.shareImgUrl,
        success: this.onShareFinish.bind(this),
        cancel: this.onShareFinish.bind(this)
    };
    wx.onMenuShareTimeline(shareInfoTimeLine);
};

MyWechat.prototype.onShareFinish = function () {
    console.log("MyWechat::onShareFinish", detailInd);
    this.shareConfig();
};

window.myWx = new MyWechat(
    [
        { title: "分享标题", desc: "分享内容", shareImgUrl: "" },
    ]
);