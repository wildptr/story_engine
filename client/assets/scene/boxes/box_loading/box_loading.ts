import Utils from "../../../core/utils";


const { ccclass, property } = cc._decorator;

@ccclass
export default class BoxLoading extends cc.Component {
    @property(cc.Label)
    lb: cc.Label = null;
    @property(cc.ProgressBar)
    pb: cc.ProgressBar = null;
    protected onDestroy(): void {
        console.log("BoxLoading::onDestroy");
        Utils.loader.off(this);
    }
    protected onLoad(): void {
        console.log("BoxLoading::onLoad");
        Utils.loader.on("log", this.onLoaderLog, this);
        Utils.loader.on("progress", this.onLoaderProgress, this);
        Utils.loader.on("update-end", this.onLoaderEnd, this);
        Utils.loader.on("load-end", this.onLoaderEnd, this);
    }

    onLoaderLog(str: string) {
        this.lb.string = str;
    }
    onLoaderProgress(percent: number) {
        this.pb.progress = percent;
    }
    onLoaderEnd() {
        this.node.destroy();
    }
};