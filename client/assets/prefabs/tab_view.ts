import { SubjectComponent } from "../core/subject";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TabView extends SubjectComponent {
    @property([cc.Node])
    pages: cc.Node[] = [];
    @property
    curInd = 0;
    onLoad() {
        this.pages.forEach((page, ind) => {
            page.active = this.curInd === ind;
        });
    }
    start() {
        this.sel(this.curInd, true);
    }
    sel(ind: number, silent = false) {
        this.curInd = ind;
        this.pages.forEach((page, ind) => {
            page.active = this.curInd === ind;
        });
        if (!silent) {
            this.emit("select", this);
        }
    }
    onClick(evt, val) {
        this.sel(Number(val));
    }
};