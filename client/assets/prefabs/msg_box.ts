import Macro from "../core/macro";
import MsgHub, { SubjectComponent } from "../core/subject";

const { ccclass, property } = cc._decorator;


export enum MsgBoxType {
    Ok,
    YesNo,
};

export class MsgBoxInfo {
    type = MsgBoxType.Ok;
    text = "";
    title = "";
}

@ccclass
export default class MsgBox extends SubjectComponent {
    @property(cc.Label)
    textLb: cc.Label = null;
    @property(cc.Label)
    titleLb: cc.Label = null;
    @property(cc.Button)
    btnOk: cc.Button = null;
    @property(cc.Button)
    btnYes: cc.Button = null;
    @property(cc.Button)
    btnNo: cc.Button = null;

    info: MsgBoxInfo = null;
    _yesCb: () => void;
    _noCb: () => void;

    setYesNoCallBack(yesCb: () => void, noCb?: () => void) {
        this._yesCb = yesCb;
        this._noCb = noCb;
    }


    setInfo(info: MsgBoxInfo) {
        this.info = info;
        this.textLb.string = this.info.text;

        this.titleLb.node.active = !!this.info.title;
        this.titleLb.string = this.info.title;

        this.btnOk.node.active = this.info.type === MsgBoxType.Ok;
        this.btnYes.node.active = this.info.type === MsgBoxType.YesNo;
        this.btnNo.node.active = this.info.type === MsgBoxType.YesNo;
    }

    onClickOk() {
        MsgHub.emit(Macro.EVENTS.MSG_BOX_OK);
        this.emit(Macro.EVENTS.MSG_BOX_OK, this);
    }

    onClickYes() {
        this._yesCb && this._yesCb();
        MsgHub.emit(Macro.EVENTS.MSG_BOX_YES);
        this.emit(Macro.EVENTS.MSG_BOX_YES, this);
    }

    onClickNo() {
        this._noCb && this._noCb();
        MsgHub.emit(Macro.EVENTS.MSG_BOX_NO);
        this.emit(Macro.EVENTS.MSG_BOX_NO, this);
    }
}
