var SoundList = [
    "dead",
    "border",
    "btn",
    "pop",
    "reward3",
    "bo",
    "bingo",
    "reward2",
];
var SoundUrl = "sounds/"
var SoundBase = function () {
    this.bgm = document.body.querySelector("audio[name=bgm]");
    this.lib = {};
    this.loadedCount = 0;
    for (var i = 0; i < SoundList.length; i++) {
        let key = SoundList[i];
        this.lib[key] = new Howl({
            src: [SoundUrl + key + ".mp3"],
            autoplay: false,
            loop: false,
            volume: 1,
            preload: true,
            html5: false,
            onload: this._onLoaded.bind(this),
            onloaderror: this._onLoaded.bind(this),
        });
    }
};
SoundBase.prototype._onLoaded = function () {
    this.loadedCount++;
    if (this.loadedCount >= SoundList.length) {
        if (this.onAllLoaded) {
            this.onAllLoaded();
        }
    }
}
SoundBase.prototype.getLoadPercent = function () {
    return this.loadedCount / SoundList.length;
};
SoundBase.prototype.mute = function () {
    this.bgm.muted = true;
    for (var key in this.lib) {
        this.lib[key].mute = true;
    }
};
SoundBase.prototype.unmute = function () {
    this.bgm.muted = false;
    for (var key in this.lib) {
        this.lib[key].mute = false;
    }
};
SoundBase.prototype.isAllLoaded = function () {
    return this.loadedCount >= SoundList;
};
SoundBase.prototype.playBgm = function () {
    this.bgm.play();
};
SoundBase.prototype.stopBgm = function () {
    this.bgm.stop();
};
SoundBase.prototype.playSound = function (name) {
    if (!this.lib[name]) {
        return;
    }
    this.lib[name].play();
};
SoundBase.prototype.stopSound = function (name) {
    if (!this.lib[name]) {
        return;
    }
    this.lib[name].stop();
};