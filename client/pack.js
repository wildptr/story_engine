
//遍历文件夹文件并压缩成JSZIP
const jszip = require("jszip");
const fs = require("fs");

(async () => {
    const zip = new jszip();
    let _read = async (zf, path) => {
        const fsDir = fs.opendirSync(path);
        let childDirent = fsDir.readSync();
        while (childDirent) {
            if (childDirent.isDirectory()) {
                let czf = zf.folder(childDirent.name);
                await _read(czf, path + childDirent.name + "/");
            }
            else {
                zf.file(childDirent.name, fs.readFileSync(path + childDirent.name));
            }
            childDirent = fsDir.readSync();
        }
        fsDir.closeSync();
    };

    await _read(zip, "build/web-mobile/");

    zip.generateAsync({ type: "nodebuffer",compression: "DEFLATE" }).then(function (content) {
        fs.writeFileSync("build/web-mobile.zip", content);
    });
})();