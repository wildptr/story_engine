import { LibraryRecord } from "../../core/library";
import SerializeAble, { SerializeClass, Serialize } from "../../core/serialize";
import { SubjectComponent } from "../../core/subject";
import Bullet from "./bullet";

const { ccclass, property } = cc._decorator;


@SerializeClass("WorldConfig")
export class WorldConfig extends SerializeAble implements LibraryRecord {
    @Serialize()
    key = "";
    @Serialize()
    name = "";
    @Serialize()
    index = 0; //排序
    @Serialize()
    desc = "";

    @Serialize()
    maxY = 9999;
    @Serialize()
    minY = -350;
    @Serialize()
    maxX = 9999;
    @Serialize()
    minX = -9999;
    @Serialize()
    gameTime = 0; //分秒

    @Serialize()
    prefab = ""; //用作测试，直接引用PREFAB
};

@ccclass
export default class World extends SubjectComponent {
    @property
    gravity = cc.v2(0, -9800);
    // @property
    // minY = 0;
    @property(cc.Node)
    bornNode: cc.Node = null; //用作测试
    config: WorldConfig = null;
    async onLoad() {
        super.onLoad && super.onLoad();
    }
    initWithConfig(config: WorldConfig) {
        this.config = config;
    }

    findBulletByUid(bulletUid: number) {
        return this.getComponentsInChildren(Bullet).find(ele => ele.bulletUid === bulletUid);
    }

    findBulletByTag(tag: string) {
        return this.getComponentsInChildren(Bullet).find(ele => ele.tag === tag);
    }
};