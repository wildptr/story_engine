import SerializeAble, { SerializeClass, Serialize } from "../core/serialize";
import { Uid } from "../core/utils";
import { LibraryRecord } from "./library";

@SerializeClass("UniqueObjectConfig")
export class UniqueObjectConfig extends SerializeAble implements LibraryRecord {
    @Serialize()
    key = "";

    static CreateUniqueObject<T extends UniqueObjectConfig>(config: T): UniqueObject<T> {
        let obj = new UniqueObject<T>();
        obj.init(
            Uid.getUid(20),
            config
        );
        return obj;
    }
};


export class UniqueObject<T extends UniqueObjectConfig> extends SerializeAble {
    @Serialize()
    uuid: string = "";
    @Serialize()
    config: T = null;

    property: Omit<T, "fromJSON" | "toJSON" | "key">;

    init(uuid: string, config: T) {
        this.uuid = uuid;
        this.config = config;
        this.property = <Omit<T, "fromJSON" | "toJSON" | "key">>{};
        let confPropertys = Object.keys(this.config);
        confPropertys.forEach(propKey => {
            if (propKey === "key" || propKey === "fromJSON" || propKey === "toJSON") {
                return;
            }
            this.property[propKey] = this.config[propKey];
        });
    }
};